\select@language {french}
\contentsline {chapter}{\numberline {1}Optique classique}{15}
\contentsline {section}{\numberline {1.1}Introduction}{15}
\contentsline {section}{\numberline {1.2}Historique}{15}
\contentsline {section}{\numberline {1.3}La r\IeC {\'e}flexion}{15}
\contentsline {subsection}{\numberline {1.3.1}Loi de la r\IeC {\'e}flexion}{15}
\contentsline {subsection}{\numberline {1.3.2}Le miroir plat}{16}
\contentsline {subsection}{\numberline {1.3.3}Le miroir sph\IeC {\'e}rique}{16}
\contentsline {subsection}{\numberline {1.3.4}Le miroir parabolique}{16}
\contentsline {subsection}{\numberline {1.3.5}Le t\IeC {\'e}lescope}{17}
\contentsline {subsubsection}{Le phare de voiture}{17}
\contentsline {subsubsection}{La lampe de poche}{17}
\contentsline {subsubsection}{Le four solaire}{17}
\contentsline {subsubsection}{L'antenne parabolique de t\IeC {\'e}l\IeC {\'e}vision}{18}
\contentsline {subsubsection}{Le r\IeC {\'e}troviseur}{18}
\contentsline {section}{\numberline {1.4}La r\IeC {\'e}fraction}{18}
\contentsline {subsection}{\numberline {1.4.1}La loi de la r\IeC {\'e}fraction}{19}
\contentsline {subsection}{\numberline {1.4.2}Le b\IeC {\^a}ton bris\IeC {\'e}}{20}
\contentsline {subsection}{\numberline {1.4.3}L'arc-en-ciel}{21}
\contentsline {subsection}{\numberline {1.4.4}Le prisme}{22}
\contentsline {subsection}{\numberline {1.4.5}Les lunettes}{23}
\contentsline {subsection}{\numberline {1.4.6}La r\IeC {\'e}flexion totale}{24}
\contentsline {subsubsection}{La fibre optique}{24}
\contentsline {subsubsection}{L'endoscope}{25}
\contentsline {section}{\numberline {1.5}Optique g\IeC {\'e}om\IeC {\'e}trique}{25}
\contentsline {subsection}{\numberline {1.5.1}Rayons principaux}{25}
\contentsline {subsection}{\numberline {1.5.2}Loi des lentilles minces}{25}
\contentsline {subsection}{\numberline {1.5.3}Grandissement}{26}
\contentsline {subsection}{\numberline {1.5.4}Grossissement}{26}
\contentsline {subsection}{\numberline {1.5.5}La puissance d'une lentille}{26}
\contentsline {subsection}{\numberline {1.5.6}Lentille convergente, divergente et loi des lentilles minces}{27}
\contentsline {subsection}{\numberline {1.5.7}Applications}{27}
\contentsline {subsubsection}{La loupe}{27}
\contentsline {subsubsection}{La lunette astronomique}{28}
\contentsline {subsubsection}{Le microscope}{28}
\contentsline {section}{\numberline {1.6}L'\oe il}{28}
\contentsline {subsection}{\numberline {1.6.1}L'\oe il normal}{29}
\contentsline {subsubsection}{Anatomie}{29}
\contentsline {subsubsection}{Fonctionnement}{29}
\contentsline {subsection}{\numberline {1.6.2}Les affections}{30}
\contentsline {subsubsection}{Myopie}{30}
\contentsline {subsubsection}{Hyperm\IeC {\'e}tropie}{31}
\contentsline {subsubsection}{Presbytie}{31}
\contentsline {subsubsection}{Astigmatisme}{31}
\contentsline {subsection}{\numberline {1.6.3}Les corrections}{32}
\contentsline {subsubsection}{Myopie}{32}
\contentsline {subsubsection}{Hyperm\IeC {\'e}tropie}{33}
\contentsline {subsubsection}{Presbytie}{33}
\contentsline {subsubsection}{Astigmatisme}{34}
\contentsline {chapter}{\numberline {2}Ondes}{35}
\contentsline {section}{\numberline {2.1}Introduction}{35}
\contentsline {section}{\numberline {2.2}Le principe de Huygens}{35}
\contentsline {section}{\numberline {2.3}D\IeC {\'e}finitions}{35}
\contentsline {subsection}{\numberline {2.3.1}L'onde aquatique}{35}
\contentsline {subsection}{\numberline {2.3.2}L'\IeC {\'e}nergie}{35}
\contentsline {subsection}{\numberline {2.3.3}Onde circulaire, plane et front d'onde}{36}
\contentsline {subsection}{\numberline {2.3.4}Onde transversale et longitudinale}{36}
\contentsline {section}{\numberline {2.4}Le principe de Fourier}{36}
\contentsline {subsection}{\numberline {2.4.1}Onde sinuso\IeC {\"\i }dale}{36}
\contentsline {subsection}{\numberline {2.4.2}D\IeC {\'e}finitions attach\IeC {\'e}es}{37}
\contentsline {subsection}{\numberline {2.4.3}Le spectre des ondes lumineuses}{38}
\contentsline {section}{\numberline {2.5}Le principe de superposition}{38}
\contentsline {subsubsection}{Exp\IeC {\'e}rience : la cuve \IeC {\`a} onde}{39}
\contentsline {section}{\numberline {2.6}Ondes simples}{39}
\contentsline {subsection}{\numberline {2.6.1}L'onde progressive}{39}
\contentsline {subsection}{\numberline {2.6.2}L'onde r\IeC {\'e}trograde}{39}
\contentsline {section}{\numberline {2.7}Le principe de Huygens}{40}
\contentsline {subsection}{\numberline {2.7.1}La propagation en ligne droite}{40}
\contentsline {subsection}{\numberline {2.7.2}La r\IeC {\'e}flexion}{40}
\contentsline {subsection}{\numberline {2.7.3}La r\IeC {\'e}fraction}{41}
\contentsline {subsection}{\numberline {2.7.4}La diffraction}{42}
\contentsline {subsection}{\numberline {2.7.5}L'interf\IeC {\'e}rence}{42}
\contentsline {section}{\numberline {2.8}Ph\IeC {\'e}nom\IeC {\`e}nes ondulatoires}{45}
\contentsline {subsection}{\numberline {2.8.1}Le battement}{45}
\contentsline {subsubsection}{Exp\IeC {\'e}rience : battement et effet Doppler}{46}
\contentsline {subsubsection}{Exp\IeC {\'e}rience : corde de guitare}{46}
\contentsline {subsection}{\numberline {2.8.2}Les ondes stationnaires}{47}
\contentsline {subsubsection}{L'onde stationnaire}{47}
\contentsline {subsubsection}{L'onde stationnaire en milieu ferm\IeC {\'e}}{47}
\contentsline {subsubsection}{Exp\IeC {\'e}rience : corde verticale}{49}
\contentsline {subsubsection}{Exp\IeC {\'e}rience : corde horizontale}{49}
\contentsline {subsubsection}{Exp\IeC {\'e}rience : figures de Chladni}{49}
\contentsline {section}{\numberline {2.9}Analyse spectrale}{49}
\contentsline {subsection}{\numberline {2.9.1}Onde stationnaire et spectre}{49}
\contentsline {subsection}{\numberline {2.9.2}Battement et spectre}{50}
\contentsline {section}{\numberline {2.10}L'effet Doppler}{50}
\contentsline {subsection}{\numberline {2.10.1}L'effet Doppler sonore}{50}
\contentsline {subsection}{\numberline {2.10.2}Source en mouvement et observateur au repos}{51}
\contentsline {subsubsection}{Source en approche}{51}
\contentsline {subsubsection}{Source en \IeC {\'e}loignement}{51}
\contentsline {subsection}{\numberline {2.10.3}Observateur en mouvement et source au repos}{51}
\contentsline {subsubsection}{Observateur en approche}{51}
\contentsline {subsubsection}{Observateur en \IeC {\'e}loignement}{52}
\contentsline {subsection}{\numberline {2.10.4}Cas g\IeC {\'e}n\IeC {\'e}ral}{52}
\contentsline {subsection}{\numberline {2.10.5}L'effet Doppler lumineux}{52}
\contentsline {subsubsection}{Exp\IeC {\'e}rience : battement et effet Doppler}{53}
\contentsline {chapter}{\numberline {A}Syst\IeC {\`e}me international (SI)}{55}
\contentsline {section}{\numberline {A.1}Introduction}{55}
\contentsline {section}{\numberline {A.2}Les unit\IeC {\'e}s choisies}{55}
\contentsline {section}{\numberline {A.3}Exemple}{56}
\contentsline {section}{\numberline {A.4}Conversions}{56}
\contentsline {section}{\numberline {A.5}Multiples et sous-multiples}{56}
\contentsline {section}{\numberline {A.6}Notation scientifique}{56}
\contentsline {section}{\numberline {A.7}Indices de r\IeC {\'e}fraction}{57}
\contentsline {section}{\numberline {A.8}Vitesse des ondes}{57}
\contentsline {section}{\numberline {A.9}Fr\IeC {\'e}quences de la gamme}{57}
\contentsline {subsection}{\numberline {A.9.1}Audible et visible}{58}
\contentsline {chapter}{\numberline {B}Le point aveugle}{59}
\contentsline {chapter}{\numberline {C}Travaux pratiques}{61}
\contentsline {section}{\numberline {C.1}La r\IeC {\'e}flexion}{62}
\contentsline {section}{\numberline {C.2}La r\IeC {\'e}fraction}{63}
\contentsline {section}{\numberline {C.3}Lentilles minces}{64}
\contentsline {section}{\numberline {C.4}Instruments d'optique}{65}
\contentsline {section}{\numberline {C.5}La cuve \IeC {\`a} onde}{66}
\contentsline {section}{\numberline {C.6}Le battement \IeC {\`a} travers Doppler}{67}
\contentsline {section}{\numberline {C.7}Le battement pour accorder une guitare}{68}
\contentsline {section}{\numberline {C.8}La corde verticale}{69}
\contentsline {section}{\numberline {C.9}Les plaquettes harmoniques}{70}
\contentsline {section}{\numberline {C.10}Les tuyaux harmoniques}{71}
\contentsline {section}{\numberline {C.11}La corde horizontale}{72}
\contentsline {section}{\numberline {C.12}Les figures de Chladni}{73}
\contentsline {chapter}{\numberline {D}Exercices}{75}
\contentsline {section}{\numberline {D.1}Probl\IeC {\`e}mes}{75}
\contentsline {subsection}{\numberline {D.1.1}R\IeC {\'e}flexion}{75}
\contentsline {subsection}{\numberline {D.1.2}R\IeC {\'e}fraction}{75}
\contentsline {subsection}{\numberline {D.1.3}Lentille mince}{76}
\contentsline {subsection}{\numberline {D.1.4}Instruments d'optique}{76}
\contentsline {subsection}{\numberline {D.1.5}L'\oe il}{77}
\contentsline {subsection}{\numberline {D.1.6}Ondes : d\IeC {\'e}finitions}{77}
\contentsline {subsection}{\numberline {D.1.7}Le battement}{78}
\contentsline {subsection}{\numberline {D.1.8}Les ondes stationnaires}{78}
\contentsline {subsection}{\numberline {D.1.9}Effet Doppler}{79}
\contentsline {section}{\numberline {D.2}Solutions}{80}
